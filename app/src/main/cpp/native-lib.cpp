#include <jni.h>
#include <string>

extern "C" JNIEXPORT jstring JNICALL
Java_id_ac_ui_cs_mobileprogramming_naufal_1ihsan_1pratama_wifiscanner_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}


extern "C" JNIEXPORT jint JNICALL
Java_id_ac_ui_cs_mobileprogramming_naufal_1ihsan_1pratama_wifiscanner_MainActivity_learnToAdd(
        JNIEnv *env,
        jobject /* this */,
        jint a,
        jint b) {
    return a + b;
}